#include <avr/io.h>

#define KeypadDATA_DDR DDRA
#define KeypadDATA_PORT PORTA
#define KeypadDATA_PIN PINA

#define ROW1 PA0
#define ROW2 PA1
#define ROW3 PA2
#define ROW4 PA3

#define COL1 PA4
#define COL2 PA5
#define COL3 PA6
#define COL4 PA7

#define ROWS 4
#define COLS 4

void KEYPAD_init();
char KEYPAD_get_char();