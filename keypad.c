#include <util/delay.h>
#include "keypad.h"

char keys[ROWS][COLS] = {{'1', '2', '3', 'A'},
						{'4', '5', '6', 'B'},
						{'7', '8', '9', 'C'},
						{'*', '0', '#', 'D'}};
uint8_t rows[ROWS] = {ROW1, ROW2, ROW3, ROW4};
uint8_t cols[COLS] = {COL1, COL2, COL3, COL4};

void KEYPAD_init()
{
	/* cols input */
    KeypadDATA_DDR &= ~(1 << COL1);
    KeypadDATA_PORT |= (1 << COL1);
    KeypadDATA_DDR &= ~(1 << COL2);
    KeypadDATA_PORT |= (1 << COL2);
    KeypadDATA_DDR &= ~(1 << COL3);
    KeypadDATA_PORT |= (1 << COL3);
    KeypadDATA_DDR &= ~(1 << COL4);
    KeypadDATA_PORT |= (1 << COL4);
    /* rows output */
    KeypadDATA_DDR |= (1 << ROW1);
    KeypadDATA_PORT |= (1 << ROW1);
    KeypadDATA_DDR |= (1 << ROW2);
    KeypadDATA_PORT |= (1 << ROW2);
    KeypadDATA_DDR |= (1 << ROW3);
    KeypadDATA_PORT |= (1 << ROW3);
    KeypadDATA_DDR |= (1 << ROW4);
    KeypadDATA_PORT |= (1 << ROW4);
}

char KEYPAD_get_char() {
	for (int i = 0; i < ROWS; i++) {
		KeypadDATA_PORT &= ~(1 << rows[i]);
		_delay_ms(5);
		for (int j = 0; j < COLS; j++) {
			if (!(KeypadDATA_PIN & (1 << cols[j]))) {
				KeypadDATA_PORT |= (1 << rows[i]);
				return keys[i][j];
			}
			_delay_ms(5);
		}
		KeypadDATA_PORT |= (1 << rows[i]);
		_delay_ms(5);
	}
	return 0;
}