#include "pwm.h"

void UP_MOTOR_set_duty_cycle(float percentage)
{
	if (percentage >= 0.0f && percentage <= 1.0f) {
		OCR0A = percentage * (1 + TOP) - 1;

	} else {
		/* duty cycle 0% */
		OCR0A = -1;
	}
}

void DOWN_MOTOR_set_duty_cycle(float percentage)
{
	if (percentage >= 0.0f && percentage <= 1.0f) {
		OCR0B = percentage * (1 + TOP) - 1;

	} else {
		/* duty cycle 0% */
		OCR0B = -1;
	}
}

void MOTORS_init()
{
	TCCR0A = 0;
	/* Fast PWM mode 
	 * Top at 0xFF
	 */
    TCCR0A |= (3 << WGM00);
    /* Clear on comp OCR0A */
    TCCR0A |= (1 << COM0A1);
    /* Clear on comp OCR0B */
    TCCR0A |= (1 << COM0B1);
    /* prescaler 1*/
    TCCR0B |= (1 << CS00);

    /* output for UP_MOTOR */
	PwmDATA_DDR |= (1 << UP_MOTOR_PIN);
	/* output for DOWN_MOTOR */
	PwmDATA_DDR |= (1 << DOWN_MOTOR_PIN);

    //UP_MOTOR_set_duty_cycle(1.0f);
    //DOWN_MOTOR_set_duty_cycle(1.0f);
}
