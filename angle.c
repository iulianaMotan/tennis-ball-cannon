#include "angle.h"

void MOTOR_ANGLE_init()
{
	AngleDATA_DDR |= (1 << AnglePIN);
	AngleDATA_DDR |= (1 << AngleDIRECTION);
	AngleDATA_PORT &= ~(1 << AnglePIN);
}

void MOTOR_ANGLE_forward()
{
	AngleDATA_PORT |= (1 << AngleDIRECTION);
	AngleDATA_PORT &= ~(1 << AnglePIN);
}

void MOTOR_ANGLE_backwards()
{
	AngleDATA_PORT &= ~(1 << AngleDIRECTION);
	AngleDATA_PORT |= (1 << AnglePIN);
}

void MOTOR_ANGLE_stop()
{
	AngleDATA_PORT &= ~(1 << AnglePIN);
	AngleDATA_PORT &= ~(1 << AngleDIRECTION);
	
}