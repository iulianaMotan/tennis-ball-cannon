#include <avr/io.h>

#define FeedingDATA_DDR DDRD
#define FeedingDATA_PORT PORTD
#define FeedingDATA_PIN PIND
#define Feeding_PIN PD6
#define f_CPU 16000000
#define PRESCALER 1024
#define OCR1B_PERIOD 15624
#define MAX_PERIOD 4

void FEEDING_init();
void FEEDING_set_period(uint8_t period);