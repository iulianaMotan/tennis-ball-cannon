#include <util/delay.h>
#include <avr/interrupt.h>
#include "stepper.h"

unsigned char step;

ISR(TIMER1_COMPA_vect)
{
	switch(step) {
		case 0:
			FeedingDATA_PORT1 &= ~(1 << Feeding_PIN1);
			FeedingDATA_PORT1 &= ~(1 << Feeding_PIN2);
			FeedingDATA_PORT2 &= ~(1 << Feeding_PIN3);
			FeedingDATA_PORT2 |= (1 << Feeding_PIN4);
			break;
		case 1:
			FeedingDATA_PORT1 &= ~(1 << Feeding_PIN1);
			FeedingDATA_PORT1 &= ~(1 << Feeding_PIN2);
			FeedingDATA_PORT2 |= (1 << Feeding_PIN3);
			FeedingDATA_PORT2 |= (1 << Feeding_PIN4);
			break;
		case 2:
			FeedingDATA_PORT1 &= ~(1 << Feeding_PIN1);
			FeedingDATA_PORT1 &= ~(1 << Feeding_PIN2);
			FeedingDATA_PORT2 |= (1 << Feeding_PIN3);
			FeedingDATA_PORT2 &= ~(1 << Feeding_PIN4);
			break;
		case 3:
			FeedingDATA_PORT1 &= ~(1 << Feeding_PIN1);
			FeedingDATA_PORT1 |= (1 << Feeding_PIN2);
			FeedingDATA_PORT2 |= (1 << Feeding_PIN3);
			FeedingDATA_PORT2 &= ~(1 << Feeding_PIN4);
			break;
		case 4:
			FeedingDATA_PORT1 &= ~(1 << Feeding_PIN1);
			FeedingDATA_PORT1 |= (1 << Feeding_PIN2);
			FeedingDATA_PORT2 &= ~(1 << Feeding_PIN3);
			FeedingDATA_PORT2 &= ~(1 << Feeding_PIN4);
			break;
		case 5:
			FeedingDATA_PORT1 |= (1 << Feeding_PIN1);
			FeedingDATA_PORT1 |= (1 << Feeding_PIN2);
			FeedingDATA_PORT2 &= ~(1 << Feeding_PIN3);
			FeedingDATA_PORT2 &= ~(1 << Feeding_PIN4);
			break;
		case 6:
			FeedingDATA_PORT1 |= (1 << Feeding_PIN1);
			FeedingDATA_PORT1 |= (1 << Feeding_PIN2);
			FeedingDATA_PORT2 |= (1 << Feeding_PIN3);
			FeedingDATA_PORT2 |= (1 << Feeding_PIN4);
			break;
		case 7:
			FeedingDATA_PORT1 |= (1 << Feeding_PIN1);
			FeedingDATA_PORT1 &= ~(1 << Feeding_PIN2);
			FeedingDATA_PORT2 &= ~(1 << Feeding_PIN3);
			FeedingDATA_PORT2 |= (1 << Feeding_PIN4);
			break;
		default:
			FeedingDATA_PORT1 &= ~(1 << Feeding_PIN1);
			FeedingDATA_PORT1 &= ~(1 << Feeding_PIN2);
			FeedingDATA_PORT2 &= ~(1 << Feeding_PIN3);
			FeedingDATA_PORT2 &= ~(1 << Feeding_PIN4);
			break;
	}
	step = (step + 1) % 8;
}

void FEEDING_init()
{

	TCCR1A = 0;
	TCCR1B = 0;
	TIMSK1 = 0;

    /* interrupt compare with OCR1A */
    TIMSK1 |= (1 << OCIE1A);
    /* CTC with TOP at OCR1A */
    TCCR1B |= (1 << WGM12);
    /* prescaler 1024*/
    TCCR1B |= (5 << CS10);
    /* 1 milisecond */
    OCR1A = PERIOD;

	FeedingDATA_DDR1 |= (1 << Feeding_PIN1);
	FeedingDATA_PORT1 &= ~(1 << Feeding_PIN1);
	FeedingDATA_DDR1 |= (1 << Feeding_PIN2);
	FeedingDATA_PORT1 &= ~(1 << Feeding_PIN2);
	FeedingDATA_DDR2 |= (1 << Feeding_PIN3);
	FeedingDATA_PORT2 &= ~(1 << Feeding_PIN3);
	FeedingDATA_DDR2 |= (1 << Feeding_PIN4);
	FeedingDATA_PORT2 &= ~(1 << Feeding_PIN4);

	/* enable interrupts */
	sei();
}
