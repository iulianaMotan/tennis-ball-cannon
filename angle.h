#include <avr/io.h>

#define AngleDATA_DDR DDRD
#define AngleDATA_PORT PORTD
#define AngleDATA_PIN PIND
#define AnglePIN PD4
#define AngleDIRECTION PD5
#define FORWARD '5'
#define BACKWARDS '4'

void MOTOR_ANGLE_init();
void MOTOR_ANGLE_forward();
void MOTOR_ANGLE_backwards();
void MOTOR_ANGLE_stop();