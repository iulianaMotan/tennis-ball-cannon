#include <avr/io.h>

#define FeedingDATA_DDR1 DDRB
#define FeedingDATA_PORT1 PORTB
#define FeedingDATA_PIN1 PINB
#define FeedingDATA_DDR2 DDRD
#define FeedingDATA_PORT2 PORTD
#define FeedingDATA_PIN2 PIND
#define Feeding_PIN1 PB0
#define Feeding_PIN2 PB1
#define Feeding_PIN3 PD0
#define Feeding_PIN4 PD1
#define f_CPU 16000000
#define PRESCALER 1024
#define PERIOD 15

void FEEDING_init();