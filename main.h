#define ENTER '#'
#define NEXTLINE 0x40
#define DELAY_START 3000
#define DELAY_END 2000

void init();
void select_angle();
void select_spin();
void select_feeding();
void start();