#include <avr/io.h>

#define TOP 0xFF
#define SPIN 0.5f
#define MAXSPIN 0.7f
#define ZEROSPIN 0.0f
#define TOPSPIN 'A'
#define BACKSPIN 'B'
#define NOSPIN 'C'

#define PwmDATA_DDR DDRB
#define PwmDATA_PORT PORTB
#define PwmDATA_PIN PINB
#define UP_MOTOR_PIN PB3
#define DOWN_MOTOR_PIN PB4

void UP_MOTOR_set_duty_cycle(float percentage);
void DOWN_MOTOR_set_duty_cycle(float percentage);
void MOTORS_init();