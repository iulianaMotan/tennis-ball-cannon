#include <util/delay.h>
#include <avr/interrupt.h>
#include "feeding.h"

unsigned char step;

/* interrupt compare OCR1A */
ISR(TIMER1_COMPA_vect)
{
	/* start the motor */
    FeedingDATA_PORT |= (1 << Feeding_PIN);
}

/* intrerrupt compare OCR1B */
ISR(TIMER1_COMPB_vect)
{
	/* stop the motor */
	FeedingDATA_PORT &= ~(1 << Feeding_PIN);
}

void FEEDING_init()
{

	TCCR1A = 0;
	TCCR1B = 0;
	TIMSK1 = 0;

    /* compare threshhold for OCR1B is 1s */
	OCR1B = OCR1B_PERIOD;
    /* intreruperea de comparare cu OCR1A */
    TIMSK1 |= (1 << OCIE1A);
    /* interrupt compare with OCR1B */
    TIMSK1 |= (1 << OCIE1B);
    /* CTC with TOP at OCR1A */
    TCCR1B |= (1 << WGM12);
    /* prescaler 1024*/
    TCCR1B |= (5 << CS10);

    FeedingDATA_DDR |= (1 << Feeding_PIN);
    FeedingDATA_PORT &= ~(1 << Feeding_PIN);
    /* enable interrupts */
	sei();
}

void FEEDING_set_period(uint8_t period) {
	OCR1A = f_CPU * period / PRESCALER - 1;
}