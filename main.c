#include <avr/io.h>
#include <util/delay.h>
#include "lcd.h"
#include "keypad.h"
#include "pwm.h"
#include "angle.h"
#include "stepper.h"
#include "main.h"

int main(void) {
	init();
	select_angle();
	select_spin();
	FEEDING_init();
	while(1) {
		LCD_writeInstr(LCD_INSTR_clearDisplay);
		LCD_print("Enjoy playing!");
		_delay_ms(100);
	}
	return 0;
}

void init()
{
	DDRA = 0;
	DDRB = 0;
	DDRC = 0;
	DDRD = 0;

	LCD_init();
	KEYPAD_init();
}

void select_angle()
{
	char c;
	
	MOTOR_ANGLE_init();
	LCD_writeInstr(LCD_INSTR_clearDisplay);
	LCD_printAt(0, "Adjust angle!");
	LCD_printAt(NEXTLINE, "4-BACK,5-FORWARD");
	_delay_ms(DELAY_START);
	LCD_writeInstr(LCD_INSTR_clearDisplay);
	LCD_printAt(0, "Adjust angle!");
	LCD_printAt(NEXTLINE, "#-DONE");
	_delay_ms(DELAY_START);
	while(1) {
		c = KEYPAD_get_char();
		if (c == ENTER) {
			LCD_print("Angle set!");
			_delay_ms(DELAY_END);
			break;
		}
		LCD_writeInstr(LCD_INSTR_clearDisplay);
		LCD_printAt(0, "Adjust angle!");
		if (c == FORWARD) {
			LCD_printAt(NEXTLINE, "Forward");
			MOTOR_ANGLE_forward();
		} else if (c == BACKWARDS) {
			LCD_printAt(NEXTLINE, "Backwards");
			MOTOR_ANGLE_backwards();
		} else {
			LCD_printAt(NEXTLINE, "");
			MOTOR_ANGLE_stop();
		}
		_delay_ms(100);
	}
	LCD_writeInstr(LCD_INSTR_clearDisplay);
}

void select_spin()
{
	char c;
	
	MOTORS_init();
	LCD_writeInstr(LCD_INSTR_clearDisplay);
	LCD_printAt(0, "Select spin!");
	LCD_printAt(NEXTLINE, "A-T, B-B, C-N");
	_delay_ms(DELAY_START);
	while(1) {
		c = KEYPAD_get_char();
		LCD_writeInstr(LCD_INSTR_clearDisplay);
		LCD_printAt(0, "Select spin!");
		if (c == TOPSPIN) {
			LCD_printAt(NEXTLINE, "Topspin");
			_delay_ms(DELAY_END);
			UP_MOTOR_set_duty_cycle(MAXSPIN);
			DOWN_MOTOR_set_duty_cycle(SPIN);
			break;
		}
		if (c == BACKSPIN) {
			LCD_printAt(NEXTLINE, "Backspin");
			_delay_ms(DELAY_END);
			UP_MOTOR_set_duty_cycle(SPIN);
			DOWN_MOTOR_set_duty_cycle(MAXSPIN);
			break;
		}
		if (c == NOSPIN) {
			LCD_printAt(NEXTLINE, "No spin");
			_delay_ms(DELAY_END);
			UP_MOTOR_set_duty_cycle(MAXSPIN);
			DOWN_MOTOR_set_duty_cycle(MAXSPIN);
			break;
		}
		if (c != 0) {
			LCD_printAt(NEXTLINE, "Wrong spin");
			_delay_ms(DELAY_END);
		}
		_delay_ms(100);
	}
	LCD_writeInstr(LCD_INSTR_clearDisplay);
}